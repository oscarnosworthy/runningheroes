var math = require('mathjs');

exports.trackEntersTheBox = function(gpsTrack, pointNorthWest, pointSouthEast){
    var found = false;
    gpsTrack.forEach(function(trackpoint){
    	if ((trackpoint.lat >= pointNorthWest.lat && trackpoint.lat <= pointSouthEast.lat)
    		&& (trackpoint.lon >= pointNorthWest.lon && trackpoint.lon <= pointSouthEast.lon))
    	{
	    found = true;
    	  return false;
    	}
    });
    return found;
};

exports.trackIsInTheBox = function(gpsTrack, pointNorthWest, pointSouthEast){
    var BreakException = {};
    try {
    gpsTrack.forEach(function(trackpoint){
    	if (trackpoint.lat < pointNorthWest.lat || trackpoint.lat > pointSouthEast.lat
    		|| trackpoint.lon < pointNorthWest.lon || trackpoint.lon > pointSouthEast.lon)
    	{
    		throw BreakException;
    	}
    }); 
	}catch (e) {
   	if (e === BreakException)
   	{
   		return false;
   	}
   	}
    return true;
};

exports.getDistanceBetweenPointAndNearestGpsPoint = function(gpsTrack, gpsPoint){
	var smallest = 2147483600;
	gpsTrack.forEach(function(trackpoint){
		var num = math.distance([trackpoint.lat, trackpoint.lon], [gpsPoint.lat, gpsPoint.lon]);
    	if (num < smallest){
    		smallest = num;
    	}
    });
    return smallest;
}
