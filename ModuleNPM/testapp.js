var load = require('runningheroespackage');

var gpsTrack =
    [
	{
	    lat:1,
	    lon:1,
	    time:6545
	},
	{
	    lat:2,
	    lon:3,
	    time:576567
	},
	{
	    lat:3,
	    lon:56,
	    time:45788
	}
    ];

var pointNorthWest =
    {
	lat:2,
	lon:2
    };

var pointSouthEast =
    {
	lat:456,
	lon:453
    };

console.log("test of the app");
console.log("result from rackEntersTheBox(gpsTrack, pointNorthWest, pointSouthEast): " + load.trackEntersTheBox(gpsTrack, pointNorthWest, pointSouthEast))
console.log("result from trackIsInTheBox(gpsTrack, pointNorthWest, pointSouthEast) : " + load.trackIsInTheBox(gpsTrack, pointNorthWest, pointSouthEast));
console.log("result from getDistanceBetweenPointAndNearestGpsPoint(gpsTrack, pointNorthWest): " + load.getDistanceBetweenPointAndNearestGpsPoint(gpsTrack, pointNorthWest));
