var myApp = angular.module('myApp',['ngRoute']);

myApp.config(function($routeProvider){
	$routeProvider.when('/', {
		controller:'UsersController',
		templateUrl: 'views/users.html'
	})
	.when('/users', {
		controller:'UsersController',
		templateUrl: 'views/users.html'
	})
	.when('/users/:id', {
		controller:'UsersController',
		templateUrl: 'views/user.html'
	})
	.when('/ranking/:id', {
		controller:'RankingController',
		templateUrl: 'views/ranking.html'
	})
	.when('/fullranking/:id', {
		controller:'RankingController',
		templateUrl: 'views/ranking.html'
	})
	.otherwise({
		redirectTo: '/'
	});
});