var myApp = angular.module('myApp');

myApp.controller('UsersController', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams){
	console.log('UsersController loaded...');
	
	$scope.getUsers = function(){
		$http.get('/api/users').then(function (response){
			$scope.users = response.data;
   		},function (error){
   			throw err;
   		});
	}
	$scope.getUser = function(){
		var id = $routeParams.id;
		$http.get('/api/users/'+id).then(function (response){
			$scope.user = response.data;
   		},function (error){
   			throw err;
   		});
	}
}]);