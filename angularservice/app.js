const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

app.use(express.static(__dirname+'/client'));
app.use(bodyParser.json());

Ranking =require('./models/ranking');
Users =require('./models/users');

mongoose.connect('mongodb://localhost/newdb');
var db = mongoose.connection;

app.get('/', (req, res) => {
	res.send('Please use /ranking or /users');
});

app.get('/api/users/:_id', (req, res) => {
	Users.getUserById(req.params._id, (err, user) => {
		if(err){
			throw err;
		}
		res.json(user);
	});
});

app.get('/api/users', (req, res) => {
	Users.getUsers((err, users) => {
		if(err){
			throw err;
		}
		res.json(users);
	});
});

app.get('/api/ranking/:_id', (req, res) => {
	// Ranking.getRankingById(req.params._id, (err, ranking) => {
	// 	if(err){
	// 		throw err;
	// 	}
	// 	res.json(ranking);
	// });
	res.json([
		{
			"position" : 1,
			"user" : 32000,
			"score" : 100
		},
		{
			"position" : 2,
			"user" : 121000,
			"score" : 89
		},
		{
			"position" : 3,
			"user" : 145000,
			"score" : 54
		}
	])
});

// app.get('/ranking', (req, res) => {
// 	Ranking.getRanking((err, ranking) => {
// 		if(err){
// 			throw err;
// 		}
// 		res.json();
// 	});
// });

// app.get('/users?ids=:res', (req, res) => {
// 	var arr = req.query.res.split(',');
// 	// Users.getUserById(req.params._id, arr, (err, user) => {
// 	// 	if(err){
// 	// 		throw err;
// 	// 	}
// 	// 	res.json(user);
// 	// });
// });

app.post('/api/users', (req, res) => {
	var user = req.body;
	Users.adduser(user, (err, user) => {
		if(err){
			throw err;
		}
		res.json(user);
	});
});

app.put('/api/users/:_id', (req, res) => {
	var id = req.params._id;
	var user = req.body;
	Users.updateUser(id, user, {}, (err, user) => {
		if(err){
			throw err;
		}
		res.json(user);
	});
});

app.delete('/api/users/:_id', (req, res) => {
	var id = req.params._id;
	Users.removeUser(id, (err, user) => {
		if(err){
			throw err;
		}
		res.json(user);
	});
});

app.listen(3000);
console.log('Running on port 3000...');
