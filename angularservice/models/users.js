const mongoose = require('mongoose');

// Ranking Schema
const usersSchema = mongoose.Schema(
	{
		id:{
			type: Number,
			required: true
		},
		firstname:{
			type: String,
			required: true
		},
		lastname:{
			type: String,
			required: true
		}
	}
);

const User = module.exports = mongoose.model('User', usersSchema);

// Get user by id
module.exports.getUserById = (id, callback) => {
	User.findById(id, callback);
}

// // Get users by ids
// module.exports.getUserById = (id, ids, callback) => {
// 	User.findById(id, callback);
// 	ids.forEach(function(id){
// 		User.findById(id, callback)
// 	});
// }

////////////////////////////////////

// Get users
module.exports.getUsers = (callback, limit) => {
	User.find(callback).limit(limit);
}

// add a user
module.exports.addUser = (user, callback) => {
	User.create(user, callback);
}

// Update user
module.exports.updateUser = (id, user, options, callback) => {
	var query = {_id: id};
	var update = {
		id:user.id,
		firstname:user.firstname,
		lastname:user.lastname
	}
	User.findOneAndUpdate(query, update, options, callback);
}

// Delete user
module.exports.removeUser = (id, callback) => {
	var query = {_id: id};
	User.remove(query, callback);
}
