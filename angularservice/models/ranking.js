const mongoose = require('mongoose');

const rankingSchema = mongoose.Schema(
	[{
		position:{
			type: Number,
			required: true
		},
		user:{
			type: Number,
			required: true
		},
		score:{
			type: Number,
			required: true
		}
	}]
);

const Ranking = module.exports = mongoose.model('Ranking', rankingSchema);

// Get ranking by id
module.exports.getRankingById = (id, callback) => {
	Ranking.findById(id, callback);
}
